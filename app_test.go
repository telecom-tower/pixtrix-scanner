package ptscanner

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"google.golang.org/appengine/aetest"
	"image"
	_ "image/png"
	"net/http/httptest"
	"os"
	"testing"
        "net/http"
)

var inst aetest.Instance

func setup(t *testing.T) {
	if inst == nil {
		var err error
		inst, err = aetest.NewInstance(nil)
		assert.NoError(t, err)
	}
}

// ---------------------  RenderImage

var toImageTests = []string{"res/matrix6", "res/it_works"}

func Test_RenderImage(t *testing.T) {
        setup(t)
        for _, filename := range toImageTests {
                checkImage(t, filename + ".json", filename + ".png", false)
                checkImage(t, filename + ".json", filename + "_realistic.png", true)
        }
}

func checkImage(t *testing.T, jsonFile, imageFile string, realistic bool){
        // read input file
        b, err := os.Open(jsonFile)
        defer b.Close()
        assert.NoError(t, err)

        // read result image
        f, err := os.Open(imageFile)
        assert.NoError(t, err)
        img, _, err := image.Decode(f)
        assert.NoError(t, err)
        f.Close()

        // check renderImage
        var req *http.Request
        w := httptest.NewRecorder()
        var uri string

        if realistic {
                uri = "/renderRealistic"
                req, err = inst.NewRequest("POST", uri, b)
                renderRealisticImage(w, req)
        }else {
                uri = "/renderImage"
                req, err = inst.NewRequest("POST", uri, b)
                renderImage(w, req)
        }

        assert.Equal(t, 200, w.Code)
        result, _, err := image.Decode(w.Body)
        assert.NoError(t, err)
        compareImages(t, img, result, uri + " --> " + jsonFile + " : " + imageFile)
}

func compareImages(t *testing.T, img, result image.Image, comment string ) {
        if !assert.Equal(t, img.Bounds(), result.Bounds(), comment) {
                return
        }
        for x := img.Bounds().Min.X; x < img.Bounds().Max.X; x++ {
                for y := img.Bounds().Min.Y; y < img.Bounds().Max.Y; y++ {
                        if !assert.Equal(t, img.At(x, y), result.At(x, y)) {
                                return
                        }
                }
        }
}

// ---------------------  ToText

type toTextTest struct {
	filename string
	result   string
	comment  string
}

var toTextTests = [...]toTextTest{
	toTextTest{"res/matrix6.json", "BlueMasters", "basic text (6x8)"},
	toTextTest{"res/matrix8.json", "BlueMasters", "basic text (8x8)"},
	toTextTest{"res/it_works.json", "♥ IT wOrKs ! ← [yeah]", "decode special symbols (8x8)"},
}

func Test_ToText(t *testing.T) {
	setup(t)
	for _, ttt := range toTextTests {
		b, err := os.Open(ttt.filename)
		assert.NoError(t, err)

		req, err := inst.NewRequest("POST", "/toText", b)
		w := httptest.NewRecorder()
		toText(w, req)

		assert.Equal(t, 200, w.Code)
		dec := json.NewDecoder(w.Body)
		var result TextResult
		dec.Decode(&result)

		assert.Equal(t, result.Text, ttt.result, ttt.comment)
	}

}

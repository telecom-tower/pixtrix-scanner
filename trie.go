package ptscanner

import (
	"encoding/gob"
	"fmt"
	"os"
)

type Trie struct {
	Children map[byte]*Trie
	Ascii    *rune
}

func NewTrie() *Trie {
	t := Trie{}
	t.Children = make(map[byte]*Trie)
	return &t
}

func LoadTrie(filename string) (t *Trie, err error) {
        dataFile, err := os.Open(filename)

        if err != nil {
                return
        }

        dataDecoder := gob.NewDecoder(dataFile)
        err = dataDecoder.Decode(&t)

        if err != nil {
                return
        }

        dataFile.Close()
        return
}

func (trie *Trie) Insert(bs []byte, ascii rune) {
	children := trie.Children
	var node *Trie
	for i, b := range bs {
		if n, ok := children[b]; ok {
			node = n
			if node.Ascii != nil {
				fmt.Sprintf("warning: 0x%s | 0x%s => 0x%04x (%#[3]c), 0x%04x (%#[4]c)\n",
					BytesToHexa(bs), BytesToHexa(bs[0:i]), ascii, *node.Ascii)
			}
		} else {
			node = &Trie{make(map[byte]*Trie), nil}
			children[b] = node
		}
		children = node.Children
	}
	node.Ascii = &ascii
}

func (t *Trie) Search(s []byte) *rune {
	children := t.Children
	var node *Trie

	for _, b := range s {
		if n, ok := children[b]; ok {
			node = n
			children = n.Children
		} else {
			return nil
		}
	}
	return node.Ascii
}

func BytesToHexa(bs []byte) string {
	s := ""
	for _, b := range bs {
		s += fmt.Sprintf("%02x", b)
	}
	return s
}

func (t *Trie) Save(filename string) (err error) {
	dataFile, err := os.Create(filename)

	if err != nil {
		return
	}

	// serialize the data
	dataEncoder := gob.NewEncoder(dataFile)
	dataEncoder.Encode(t)
	dataFile.Close()
	return
}


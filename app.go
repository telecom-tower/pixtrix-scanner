package ptscanner

import (
        "encoding/json"
        "fmt"
        "github.com/gorilla/mux"
        "image"
        "image/png"
        "net/http"
        "bytes"
        "strconv"
        "errors"
        "gitlab.com/telecom-tower/pixtrix"
)

const (
        DEFAULT_PIXSIZE = 4
)

func init() {
        router := mux.NewRouter().StrictSlash(true)
        router.HandleFunc("/renderImage", renderImage)
        router.HandleFunc("/renderRealistic", renderRealisticImage)
        router.HandleFunc("/toText", toText)
        http.Handle("/", router)
        //http.ListenAndServe(":8080", nil)
}

type TextResult struct {
        Text string `json:"text"`
}

func toText(w http.ResponseWriter, r *http.Request) {
        // get matrix
        var matrix pixtrix.Pixtrix
        err := decodePixtrix(r, &matrix)

        if err != nil {
                http.Error(w, fmt.Sprintf("Invalid JSON: %v", err), 400)
                return
        }

        txt := ParseText(&matrix)
        data := TextResult{txt}
        enc := json.NewEncoder(w)
        enc.Encode(data)
}

// renderImage renders a "normal" image
func renderImage(w http.ResponseWriter, r *http.Request) {

        // get optional  query parameter
        pixSize := DEFAULT_PIXSIZE
        err := getPixSizeParam(r, &pixSize)
        if err != nil {
                http.Error(w, err.Error(), 400)
        }

        // get matrix
        var matrix pixtrix.Pixtrix
        err = decodePixtrix(r, &matrix)

        if err != nil {
                http.Error(w, fmt.Sprintf("Invalid JSON: %v", err), 400)
                return
        }

        m := GenerateImage(&matrix, pixSize)
        writeImage(w, m)
}

// renderImage renders a realistic image
func renderRealisticImage(w http.ResponseWriter, r *http.Request) {

        // get optional  query parameter
        pixSize := DEFAULT_PIXSIZE
        err := getPixSizeParam(r, &pixSize)
        if err != nil {
                http.Error(w, err.Error(), 400)
        }

        // get matrix
        var matrix pixtrix.Pixtrix
        err = decodePixtrix(r, &matrix)

        if err != nil {
                http.Error(w, fmt.Sprintf("Invalid JSON: %v", err), 400)
                return
        }

        m := GenerateRealisticImage(&matrix, pixSize)
        writeImage(w, m)
}

// -------------- utils

// getPixSizeParam parses the optional pixSize parameter
// if not given, the pixSize parameter is left intact
// if given but incorrect, it will return an error
func getPixSizeParam(r *http.Request, pixSize *int) error {
        param := r.FormValue("pixSize")

        if param != "" {
                size, err := strconv.Atoi(param)
                if err != nil || size <= 0 || size >= 30 {
                        return errors.New(fmt.Sprintf("Invalid  parameter (should be an int between 1 and 30): %s", param))
                }
                *pixSize = size
        }

        return nil
}

// decodeMatrix parses the body of the request and converts the
// json to a matrix struct. If the json is incorrect, it will return an error
func decodePixtrix(r *http.Request, matrix *pixtrix.Pixtrix) (err error) {
        d := json.NewDecoder(r.Body)
        defer r.Body.Close()
        err = d.Decode(&matrix)
        return
}

// writeImage encodes an image 'img' in jpeg format and writes it into ResponseWriter.
func writeImage(w http.ResponseWriter, img *image.RGBA) {

        buffer := new(bytes.Buffer)
        if err := png.Encode(buffer, img); err != nil {
                http.Error(w, fmt.Sprintf("Unable to encode resulting image: %v", err), 400)
        }

        w.Header().Set("Content-Type", "image/png")
        w.Header().Set("Content-Length", strconv.Itoa(len(buffer.Bytes())))

        if _, err := w.Write(buffer.Bytes()); err != nil {
                http.Error(w, fmt.Sprintf("Unable to write resulting image: %v", err), 400)
        }
}


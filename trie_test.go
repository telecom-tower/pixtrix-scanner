package ptscanner

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var items = map[rune][]byte{
	'1': {0x42, 0x7f, 0x40, 0x00},
	'a': {0x20, 0x54, 0x54, 0x54, 0x78},
	'A': {0x7e, 0x11, 0x11, 0x11, 0x7e},
}

func createTrie() *Trie {
	t := NewTrie()

	for k, v := range items {
		t.Insert(v, k)
	}
	return t
}

func Test_Gob(t *testing.T) {
	trie := createTrie()
	trie.Save("/tmp/trie.gob")
	q, _ := LoadTrie("/tmp/trie.gob")
	r := q.Search(items['a'])
	assert.Equal(t, *r, 'a', "found key")
}

func Test_Trie(t *testing.T) {
	trie := createTrie()
	var r *rune
	for k, v := range items {
		r = trie.Search(v)
		assert.Equal(t, *r, k, "item found.")
	}
	r = trie.Search([]byte{0x11})
	assert.Nil(t, r, "not found")
	r = trie.Search([]byte{0x20, 0x54, 0x54, 0x54, 0x74})
	assert.Nil(t, r, "not found")
}

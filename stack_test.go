package ptscanner

import (
        "testing"
        "github.com/stretchr/testify/assert"
)

func Test_Stack(t *testing.T) {
        items := []Alternative{{1, "a"}, {2, "b"}, {3, "c"}, {4, "d"}}
        s := NewStack()
        for _, i := range (items) {
                s.Push(i)
        }

        for i := len(items) - 1; i >= 0; i-- {
                v, _ := s.Pop()
            assert.Equal(t, v, items[i])
        }
        assert.True(t, s.IsEmpty())
}
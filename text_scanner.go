package ptscanner

import (
	"fmt"
	"gitlab.com/telecom-tower/font"
	"gitlab.com/telecom-tower/pixtrix"
)

var t *Trie

func init() {
	var err error
	t, err = LoadTrie("trie.gob")
	if err != nil {
		t = initTrie()
	}
}

// try to decode the matrix into a text
func ParseText(m *pixtrix.Pixtrix) string {
	bg := guessBgColor(m)
	text := parseText(m, bg)
	fmt.Printf("ParseText: bg=%d, text=%s\n", bg, text)
	if len(text) < 2 {
		for _, c := range distinctColors(m) {
			if c != bg {
				fmt.Printf("ParseText: bg=%d, text=%s\n", bg, text)
				t := parseText(m, c)
				if len(t) > len(text) {
					text = t
				}
			}
		}
	}
	return text
}

func parseText(m *pixtrix.Pixtrix, bg uint32) string {

	bs := toBytes(m, bg)
	result := ""
	alternatives := NewStack() // stack of results
	children := t.Children     // current trie node
	zeroes := 0                // number of skipped zeroes

	// for debug purpose
	currentSearch := make([]byte, 9)
	currentSearchIdx := 0

	for i := 0; i < len(bs); i++ {
		b := bs[i]
		if zeroes >= 0 && b == 0 {
			// skip leading zeroes
			zeroes++
			continue
		}

		if zeroes >= 6 {
			// we skipped many zeroes, add a space
			result += " "
		}

		zeroes = -1 // we are currently searching for a symbol
		// debug: keep track of the symbol we search
		currentSearch[currentSearchIdx] = b
		currentSearchIdx++

		if n, ok := children[b]; ok {
			// go deeper in the trie
			children = n.Children
			if n.Ascii != nil {
				if i == len(bs)-1 {
					// this is the last byte in the matrix
					// don't bother looking for alternatives
					result = fmt.Sprintf("%s%c", result, *n.Ascii)
				} else {
					// this is a "result" node: keep track of the result and the index
					alternatives.Push(Alternative{i, fmt.Sprintf("%s%c", result, *n.Ascii)})
				}
			}

		} else {
			// search is done.
			if !alternatives.IsEmpty() {
				// we found one or several result during the search,
				// pop the latest one
				a, _ := alternatives.Pop()
				i = a.Idx
				result = a.Result
			} else {
				// no "result" nodes we encountered during our descent in the trie...
				//fmt.Printf("Error, undefined symbol(0x%s...)\n",
				//        BytesToHexa(currentSearch[:currentSearchIdx]))
			}
			// reset the debug variables
			currentSearchIdx = 0
			children = t.Children
			zeroes = 0 // back to skipping leading zeroes
		}
	}

	return result
}

// =====================================

// create a trie from the font 6x8 and 8x8, and save it for later
func initTrie() *Trie {

	t := NewTrie()

	for _, fnt := range []font.Font{font.Font6x8, font.Font8x8} {
		for r, bitmap := range fnt.Bitmap {
			k := getKey(bitmap)
			if k != nil {
				t.Insert(k, r)
			}
		}
	}

	t.Save("trie.gob")
	return t
}

// =====================================

func distinctColors(m *pixtrix.Pixtrix) []uint32 {
	colors := make([]uint32, 0)
	mp := make(map[uint32]bool, 0)
	for _, c := range m.Bitmap {
		if !mp[c] {
			mp[c] = true
			colors = append(colors, c)
		}
	}
	return colors
}

func guessBgColor(m *pixtrix.Pixtrix) uint32 {
	idx := (m.Rows - 1) * m.Columns()
	bg := m.Bitmap[idx+1]
	return bg
}

// convert the matrix's bitmap to a byte array
func toBytes(m *pixtrix.Pixtrix, bg uint32) []byte {
	bs := make([]byte, len(m.Bitmap)/8)
	idx := 0
	for i := 0; i < len(m.Bitmap); i += 8 {
		slice := sliceToByte(m.Bitmap[i:i+8], bg)
		bs[idx] = slice
		idx++
	}
	return bs
}

// convert an array of 8 to a byte
// remember: in the matrix bitmap, each item is equivalent to a bit
func sliceToByte(bs []uint32, bg uint32) byte {
	b := byte(0)
	for i := uint(0); i < 8; i++ {
		if bs[i] != bg {
			b |= 1 << i
		}
	}
	return b
}

// skip the leading 0 bytes of a byte array
func getKey(bitmap []byte) []byte {
	for i, b := range bitmap {
		if b != 0 {
			return bitmap[i:]
		}
	}
	return nil
}

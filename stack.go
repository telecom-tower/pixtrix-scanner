package ptscanner

import (
        "sync"
        "errors"
)

type Alternative struct {
        Idx    int
        Result string
}

type stack struct {
        lock sync.Mutex // thread safety
        s    []Alternative
}

func NewStack() *stack {
        return &stack{sync.Mutex{}, make([]Alternative, 0), }
}

func (s *stack) Push(v Alternative) {
        s.lock.Lock()
        defer s.lock.Unlock()

        s.s = append(s.s, v)
}

func (s *stack) Pop() (res Alternative, err error) {
        s.lock.Lock()
        defer s.lock.Unlock()

        l := len(s.s)
        if l == 0 {
                err = errors.New("Empty Stack")
                return
        }

        res = s.s[l - 1]
        s.s = s.s[:l - 1]
        return
}

func (s *stack) IsEmpty() bool {
        return len(s.s) == 0
}

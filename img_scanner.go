package ptscanner

import (
        "image"
        "image/color"
        "image/draw"
        "math"
        "gitlab.com/telecom-tower/pixtrix"
)

var STROKE_COLOR color.RGBA = color.RGBA{50, 50, 50, 185}

// toRGB converts a uint32 encoded color to an RGBA color
func toRGB(c uint32) color.RGBA {
        return color.RGBA{
                uint8(c >> 16),
                uint8(c >> 8),
                uint8(c),
                255,
        }
}

// -------------- realistic image

func GenerateRealisticImage(matrix *pixtrix.Pixtrix, pixSize int) (m *image.RGBA) {

        // ensure pixSize if odd, so we can center our circles
        if pixSize % 2 == 0 {
                pixSize++
        }

        h, w := matrix.Rows, matrix.Columns()
        radius := int(pixSize / 2)

        m = image.NewRGBA(image.Rect(0, 0, w * pixSize, h * pixSize))
        draw.Draw(m, m.Bounds(), &image.Uniform{color.RGBA{0, 0, 0, 255}}, image.ZP, draw.Src)
        for x := 0; x < w; x++ {
                for y := 0; y < h; y++ {
                        encodedColor := matrix.GetPixel(x, y)
                        color := toRGB(encodedColor)
                        fillCircle(m, &color, x * pixSize + radius, y * pixSize + radius, radius)
                        drawCircle(m, &STROKE_COLOR, x * pixSize + radius, y * pixSize + radius, radius)
                }
        }

        return
}

func drawCircle(m *image.RGBA, c *color.RGBA, centerX, centerY, radius int) {

        l := int(float64(radius) * math.Cos(math.Pi / 4))

        for x := 0; x <= l; x++ {
                y := int(math.Sqrt(float64(radius * radius) - float64(x * x)))
                m.Set(centerX + x, centerY + y, c)
                m.Set(centerX + x, centerY - y, c)
                m.Set(centerX - x, centerY + y, c)
                m.Set(centerX - x, centerY - y, c)

                m.Set(centerX + y, centerY + x, c)
                m.Set(centerX + y, centerY - x, c)
                m.Set(centerX - y, centerY + x, c)
                m.Set(centerX - y, centerY - x, c)
        }
}

func fillCircle(img *image.RGBA, c *color.RGBA, centerX, centerY, radius int) {
        radSquare := radius * radius
        for x := -radius; x <= radius; x++ {
                for y := -radius; y <= radius; y++ {
                        if (x * x) + (y * y) <= radSquare {
                                img.Set(centerX + x, centerY + y, c);
                        }
                }
        }
}

// -------------- "normal" image

func GenerateImage(matrix *pixtrix.Pixtrix, pixSize int) *image.RGBA {

        var h, w int = matrix.Rows, matrix.Columns()

        m := image.NewRGBA(image.Rect(0, 0, w * pixSize, h * pixSize))
        for x := 0; x < w; x++ {
                for y := 0; y < h; y++ {
                        u := matrix.GetPixel(x, y)
                        c := toRGB(u)
                        setOnePixel(m, &c, x * pixSize, y * pixSize, pixSize)
                }
        }

        return m
}

func setOnePixel(m *image.RGBA, c *color.RGBA, offsetX, offsetY, pixSize int) {
        for x := 0; x < pixSize; x++ {
                for y := 0; y < pixSize; y++ {
                        m.Set(offsetX + x, offsetY + y, c)
                }
        }
}
